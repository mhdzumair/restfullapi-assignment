module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isCustomer } = require('../middlewares/customer')

  const CustomerController = require('../controllers/customer')

  app.get('/customer/vehicles', [Auth, isCustomer], CustomerController.viewVehicles)
  app.get('/customer/appoinments', [Auth, isCustomer], CustomerController.viewAppoinments)
  app.get('/customer/services', [Auth, isCustomer], CustomerController.viewServices)
  app.get('/customer/appoinment/:id', [Auth, isCustomer], CustomerController.viewAppoinmentById)

  app.post('/customer/add_vehicle', [Auth, isCustomer], CustomerController.addVehicle)
  app.post('/customer/search_services_category', [Auth, isCustomer], CustomerController.searchServicesCategory)
  app.post('/customer/add_appoinment', [Auth, isCustomer], CustomerController.addAppoinment)

  app.put('/customer/vehicle/:id', [Auth, isCustomer], CustomerController.updateVehicle)

  app.delete('/customer/vehicle/:id', [Auth, isCustomer], CustomerController.deleteVehicle)
  app.delete('/customer/cancel_appoinment/:id', [Auth, isCustomer], CustomerController.cancelAppoinment)
}
