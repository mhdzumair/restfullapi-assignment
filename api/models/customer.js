const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VehicleId = new Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vehicle'
  }
})

const CustomerModelSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User field is required!']
  },
  vehicles: [VehicleId],
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Customer = mongoose.model('Customer', CustomerModelSchema)
module.exports = { Customer }
