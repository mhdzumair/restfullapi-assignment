const CarColors = {
  SILVER: 'SILVER',
  WHITE: 'WHITE',
  BLACK: 'BLACK',
  RED: 'RED',
  BLUE: 'BLUE',
  YELLOW: 'YELLOW',
  GREEN: 'GREEN',
  GRAY: 'GRAY',
  ORANGE: 'ORANGE'
}

module.exports = Object.freeze(CarColors)
