const mongoose = require('mongoose')
const Schema = mongoose.Schema
const VehicleType = require('../enums/vehicleTypes')
const CarColors = require('../enums/carColors')

const VehicleModelSchema = new Schema({
  vehicle_type: {
    type: String,
    enum: VehicleType,
    required: [true, 'vehicle_type field is required!']
  },
  vehicle_model: {
    type: String,
    required: [true, 'vehicle_model field is required!']
  },
  vehicle_number: {
    type: String,
    min: 3,
    required: [true, 'vehicle_number field is required!'],
    unique: true
  },
  color: {
    type: String,
    required: [true, 'vehicle color field is required!'],
    enum: CarColors
  },
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Vehicle = mongoose.model('Vehicle', VehicleModelSchema)
module.exports = { Vehicle }
