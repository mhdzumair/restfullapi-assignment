const { User } = require('../models/user')
const response = require('./response')

async function registerUser (req, res, userRoleModel, userRole) {
  req.body.role = userRole
  const user = new User(req.body)

  await user.save((err, _doc) => {
    if (err) {
      return response.BadRequest(res, err.message, err)
    }
    req.body.user = user._id
    const newUserRole = userRoleModel(req.body)
    newUserRole.save((err, _doc) => {
      if (err) {
        user.delete()
        return response.BadRequest(res, err.message, err)
      }
      return response.Created(res, 'Successfully Signed Up!')
    })
  })
}

async function getUsers (res, userModel) {
  await userModel.find((err, users) => {
    if (err) {
      return response.InternalServerError(res, 'error while retrieving user data', err)
    }
    if (!users.length) {
      return response.NotFound(res, 'no user data found')
    }

    let count = 0
    users.forEach(user => {
      user.populate('user', (_err) => {
        count += 1
        if (users.length === count) {
          return response.Ok(res, 'found users', users)
        }
      })
    })
    return users
  })
}

async function getUser (req, res, userModel) {
  await userModel.findOne({ $or: [{ _id: req.params.id }, { user: req.params.id }] }, (err, user) => {
    if (err || !user) {
      return response.BadRequest(res, 'no user data found')
    }

    user.populate('user', (_err) => {
      return response.Ok(res, 'user found!', user)
    })
  })
}

async function deleteUser (req, res, userModel) {
  await userModel.findOneAndDelete({ $or: [{ _id: req.params.id }, { user: req.params.id }] }, (err, user) => {
    if (err || !user) {
      return response.BadRequest(res, 'no user data found')
    }

    User.deleteOne({ _id: user.user }, () => {
      return response.Ok(res, 'user account deleted!')
    })
  })
}

async function updateUser (req, res, userModel) {
  await userModel.findOneAndUpdate({ $or: [{ _id: req.params.id }, { user: req.params.id }] }, req.body, { new: true }, async (err, roleUser) => {
    if (err || !roleUser) {
      return response.BadRequest(res, 'no user data found')
    }

    User.findOneAndUpdate({ _id: roleUser.user }, req.body, { new: true }, (err, user) => {
      if (err) {
        return response.UnprocessableEntity(res, 'Cant find data in user collection')
      }
      return response.Ok(res, 'user details updated!', { roldeUser: roleUser, user: user })
    })
  })
}

module.exports = {
  registerUser,
  getUsers,
  getUser,
  deleteUser,
  updateUser
}
