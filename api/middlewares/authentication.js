const { User } = require('../models/user')
const response = require('../utils/response')

const Auth = (req, res, next) => {
  let token = req.header('auth')

  if (token) {
    if (token.startsWith('Bearer')) {
      token = token.slice(7, token.length)
    }

    User.findByToken(token, (err, user) => {
      if (err || !user) {
        return response.Forbidden(res, 'Invalid token')
      }

      req.token = token
      req.user = user

      next()
    })
  } else {
    response.NetworkAuthenticationRequired(res, 'No valid token provided!')
  }
}

module.exports = { Auth }
