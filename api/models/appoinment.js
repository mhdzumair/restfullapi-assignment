const mongoose = require('mongoose')

const AppoinmentStatus = require('../enums/appoinmentStaus')

const Schema = mongoose.Schema

const AppoinmentModelSchema = new Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
    required: [true, 'Customer field is required!']
  },
  vehicle: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vehicle',
    required: [true, 'vehicle field is required!']
  },
  service_category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ServiceCategory',
    required: [true, 'service_category field is required!']
  },
  status: {
    type: String,
    enum: AppoinmentStatus,
    default: AppoinmentStatus.PENDING
  },
  schedule_date: {
    type: Date,
    required: [true, 'Schedule date is required']
  }
})

AppoinmentModelSchema.plugin(require('mongoose-id-validator'))
const Appoinment = mongoose.model('Appoinment', AppoinmentModelSchema)
module.exports = { Appoinment }
