const { User } = require('../models/user')
const { Agent } = require('../models/agent')
const { Admin } = require('../models/admin')
const { Customer } = require('../models/customer')
const UserRole = require('../enums/userRoles')
const { registerUser } = require('../utils/userManage')
const response = require('../utils/response')

exports.registerCustomer = async (req, res) => {
  await registerUser(req, res, Customer, UserRole.CUSTOMER)
}

exports.registerAgent = async (req, res) => {
  await registerUser(req, res, Agent, UserRole.AGENT)
}

exports.registerAdmin = async (req, res) => {
  await registerUser(req, res, Admin, UserRole.ADMIN)
}

exports.loginUser = (req, res) => {
  User.findOne({
    $or: [
      { email: req.body.email },
      { username: req.body.username }
    ]
  }, (err, user) => {
    if (err || !user) {
      return response.PreconditionFailed(res, 'Username or email not found!')
    } else {
      user.comparePassword(req.body.password, (_err, isMatch) => {
        // isMatch is eaither true or false
        if (!isMatch) {
          return response.Forbidden(res, 'Wrong Password!')
        } else {
          user.generateToken((err, token) => {
            if (err) {
              return response.UnprocessableEntity(res, 'Cant generate token', err)
            } else {
              return response.Ok(res, 'Successfully Logged In!', { token: token })
            }
          })
        }
      })
    }
  })
}
