module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isAdmin } = require('../middlewares/admin')

  const AdminController = require('../controllers/admin')

  app.get('/admin/admins', [Auth, isAdmin], AdminController.getAdmins)
  app.get('/admin/admin/:id', [Auth, isAdmin], AdminController.getAdminById)

  app.put('/admin/customer/:id', [Auth, isAdmin], AdminController.updateCustomer)
  app.put('/admin/agent/:id', [Auth, isAdmin], AdminController.updateAgent)

  app.delete('/admin/customer/:id', [Auth, isAdmin], AdminController.deleteCustomer)
  app.delete('/admin/agent/:id', [Auth, isAdmin], AdminController.deleteAgent)
}
