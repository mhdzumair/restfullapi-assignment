const AppoinmentStatus = {
  ACCEPTED: 'ACCEPTED',
  DECLINED: 'DECLINED',
  PENDING: 'PENDING'
}

module.exports = Object.freeze(AppoinmentStatus)
