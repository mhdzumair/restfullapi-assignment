const VehicleType = {
  CAR: 'CAR',
  VAN: 'VAN',
  LORRY: 'LORRY',
  BIKE: 'BIKE',
  BUS: 'BUS',
  THREEWHEEL: 'THREEWHEEL'
}

module.exports = Object.freeze(VehicleType)
