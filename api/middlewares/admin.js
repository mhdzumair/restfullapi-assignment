const UserRole = require('../enums/userRoles')
const response = require('../utils/response')

const isAdmin = (req, res, next) => {
  if (req.user.role === UserRole.ADMIN) {
    next()
  } else {
    response.Forbidden(res, 'No authorization to access this route!')
  }
}

module.exports = { isAdmin }
