const sendResponse = (res, status, success, message, data) => {
  if (data) {
    res.status(status).json({
      success: success,
      message: message,
      data: data
    })
  } else {
    res.status(status).json({
      success: success,
      message: message
    })
  }
}

exports.Ok = (res, message, data) => {
  sendResponse(res, 200, true, message, data)
}

exports.Created = (res, message, data) => {
  sendResponse(res, 201, true, message, data)
}

exports.Accepted = (res, message, data) => {
  sendResponse(res, 202, true, message, data)
}

exports.BadRequest = (res, message, data) => {
  sendResponse(res, 400, false, message, data)
}

exports.UnAuthorized = (res, message, data) => {
  sendResponse(res, 401, false, message, data)
}

exports.Forbidden = (res, message, data) => {
  sendResponse(res, 403, false, message, data)
}

exports.NotFound = (res, message, data) => {
  sendResponse(res, 404, false, message, data)
}

exports.MethodNotAllowed = (res, message, data) => {
  sendResponse(res, 405, false, message, data)
}

exports.NotAcceptable = (res, message, data) => {
  sendResponse(res, 406, false, message, data)
}

exports.NotAcceptable = (res, message, data) => {
  sendResponse(res, 406, false, message, data)
}

exports.PreconditionFailed = (res, message, data) => {
  sendResponse(res, 415, false, message, data)
}

exports.UnprocessableEntity = (res, message, data) => {
  sendResponse(res, 412, false, message, data)
}

exports.InternalServerError = (res, message, data) => {
  sendResponse(res, 500, false, message, data)
}

exports.NetworkAuthenticationRequired = (res, message, data) => {
  sendResponse(res, 511, false, message, data)
}
