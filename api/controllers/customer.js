const { Service } = require('../models/service')
const { ServiceCategory } = require('../models/serviceCategory')
const { Vehicle } = require('../models/vehicle')
const { Appoinment } = require('../models/appoinment')
const { searchModelData } = require('../utils/model')
const response = require('../utils/response')

const AppoinmentStaus = require('../enums/appoinmentStaus')

exports.searchServicesCategory = (req, res) => {
  const searchString = req.body.term
  if (!searchString) {
    return response.PreconditionFailed(res, 'Serach term is required!')
  }
  searchModelData(res, ServiceCategory, { category: { $regex: searchString, $options: 'i' } })
}

exports.addVehicle = async (req, res) => {
  const newVehicle = new Vehicle(req.body)

  newVehicle.customer = req.customer._id

  newVehicle.save((err, vehicle) => {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to create vehicle!', err)
    }
    req.customer.updateOne({ $push: { vehicles: { _id: vehicle._id } } }, (err, doc) => {
      if (err || !doc.nModified) {
        newVehicle.delete()
        return response.UnprocessableEntity(res, 'Unable to create vehicle!', err)
      }
      return response.Created(res, 'New vehicle is created!', vehicle)
    })
  })
}

exports.viewVehicles = async (req, res) => {
  req.customer.populate('vehicles._id', (err) => {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to retrive vehicle data', err)
    }
    return response.Ok(res, 'Received vehicles!', req.customer.vehicles)
  })
}

exports.updateVehicle = async (req, res) => {
  await Vehicle.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, function (err, vehicle) {
    if (err || !vehicle) {
      return response.UnprocessableEntity(res, 'Unable to retrive vehicle data', err)
    }
    return response.Ok(res, 'vehicle updated!', vehicle)
  })
}

exports.deleteVehicle = async (req, res) => {
  await Vehicle.findOneAndDelete({ _id: req.params.id }, function (err, vehicle) {
    if (err || !vehicle) {
      return response.UnprocessableEntity(res, 'Invalid vehicle id!', err)
    }

    req.customer.updateOne({ $pull: { vehicles: { _id: vehicle._id } } }, (err, doc) => {
      if (err || !doc.nModified) {
        return response.UnprocessableEntity(res, 'failed to pull vehicle', err)
      }

      return response.Accepted(res, 'Vehicle entry deleted!')
    })
  })
}

exports.addAppoinment = async (req, res) => {
  const newAppoinment = new Appoinment(req.body)

  newAppoinment.customer = req.customer._id

  newAppoinment.save((err, appoinment) => {
    if (err) {
      return response.BadRequest(res, 'Unable to create Appoinment!', err)
    }
    return response.Created(res, 'New Appoinment!', appoinment)
  })
}

exports.viewAppoinmentById = async (req, res) => {
  await Appoinment.findById(req.params.id, function (err, appoinment) {
    if (err || !appoinment) {
      return response.BadRequest(res, 'Invalid Appoinment id!', err)
    }

    if (appoinment.customer.toString() !== req.customer._id.toString()) {
      return response.Forbidden(res, 'UnAuthorized Appoinment id!', err)
    }

    return response.Ok(res, 'Appoinment received!', appoinment)
  })
}

exports.viewAppoinments = async (req, res) => {
  Appoinment.find({ customer: req.customer._id }, (err, appoinments) => {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to  retirive Appoinment', err)
    }
    return response.Ok(res, 'Appoinment received!', appoinments)
  })
}

exports.viewServices = async (req, res) => {
  Service.find({ customer: req.customer._id }, (err, services) => {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to  retirive services', err)
    }
    return response.Ok(res, 'services received!', services)
  })
}

exports.cancelAppoinment = async (req, res) => {
  await Appoinment.findById(req.params.id, function (err, appoinment) {
    if (err || !appoinment) {
      return response.UnprocessableEntity(res, 'Invalid Appoinment id!', err)
    }
    if (appoinment.status !== AppoinmentStaus.PENDING) {
      return response.MethodNotAllowed(res, 'You can only cancel pending appoinment', appoinment)
    }

    appoinment.remove().then(() => {
      return response.Accepted(res, 'Appoinment canceld!')
    })
  })
}
