const { Service } = require('../models/service')
const { ServiceCategory } = require('../models/serviceCategory')
const { Appoinment } = require('../models/appoinment')
const { User } = require('../models/user')
const { Vehicle } = require('../models/vehicle')
const { Customer } = require('../models/customer')
const { Agent } = require('../models/agent')
const { Admin } = require('../models/admin')

const response = require('../utils/response')
const UserRole = require('../enums/userRoles')

const { getUsers, getUser, updateUser } = require('../utils/userManage')
const { searchModelData } = require('../utils/model')

exports.getCustomers = (req, res) => {
  return getUsers(res, Customer)
}

exports.getCustomerById = async (req, res) => {
  getUser(req, res, Customer)
}

exports.getAgents = (req, res) => {
  getUsers(res, Agent)
}

exports.getAgentById = async (req, res) => {
  getUser(req, res, Agent)
}

exports.getUserDetails = (req, res) => {
  req.params.id = req.user._id

  if (req.user.role === UserRole.ADMIN) return getUser(req, res, Admin)
  else if (req.user.role === UserRole.AGENT) return getUser(req, res, Agent)
  else return getUser(req, res, Customer)
}

exports.updateUserDetails = async (req, res) => {
  req.params.id = req.user._id
  if (req.user.role === UserRole.ADMIN) return updateUser(req, res, Admin)
  else if (req.user.role === UserRole.AGENT) return updateUser(req, res, Agent)
  else return updateUser(req, res, Customer)
}

exports.getAllServiceCategory = (req, res) => {
  ServiceCategory.find(function (err, serviceCategory) {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to retrive service Category!', err)
    }
    return response.Ok(res, 'Received service Category!', serviceCategory)
  })
}

exports.getAllServices = async (req, res) => {
  await Service.find(function (err, services) {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to retrive service Category!', err)
    }
    return response.Ok(res, 'Received service Category!', services)
  })
}

exports.getServiceById = async (req, res) => {
  await Service.findById(req.params.id, async function (err, service) {
    if (err || !service) {
      return response.UnprocessableEntity(res, 'Invalid service id!', err)
    }

    return response.Ok(res, 'Received service Category!', service)
  })
}

exports.viewAppoinments = async (req, res) => {
  Appoinment.find((err, appoinments) => {
    if (err) {
      return response.UnprocessableEntity(res, 'Unable to retrive Appoinment data', err)
    }
    return response.Ok(res, 'Received Appoinments!', appoinments)
  })
}

exports.viewAppoinmentById = async (req, res) => {
  await Appoinment.findById(req.params.id, function (err, appoinment) {
    if (err || !appoinment) {
      return response.UnprocessableEntity(res, 'Invalid Appoinment id!', err)
    }
    return response.Ok(res, 'Received Appoinment!', appoinment)
  })
}

exports.searchCustomer = (req, res) => {
  const searchString = req.body.term
  if (!searchString) {
    return response.PreconditionFailed(res, 'Serach term is required!')
  }
  searchModelData(res, User, {
    $and: [{
      $or: [
        { username: { $regex: searchString, $options: 'i' } },
        { name: { $regex: searchString, $options: 'i' } }
      ]
    },
    { role: UserRole.CUSTOMER }
    ]
  })
}

exports.searchCustomer = (req, res) => {
  const searchString = req.body.term
  if (!searchString) {
    return response.PreconditionFailed(res, 'Serach term is required!')
  }
  searchModelData(res, User, {
    $and: [{
      $or: [
        { username: { $regex: searchString, $options: 'i' } },
        { name: { $regex: searchString, $options: 'i' } }
      ]
    },
    { role: UserRole.CUSTOMER }
    ]
  })
}

exports.searchVehicle = (req, res) => {
  const searchString = req.body.term
  if (!searchString) {
    return response.PreconditionFailed(res, 'Serach term is required!')
  }

  searchModelData(res, Vehicle, {
    $or: [
      { vehicle_model: { $regex: searchString, $options: 'i' } },
      { vehicle_number: { $regex: searchString, $options: 'i' } }
    ]
  })
}

exports.viewCustomerVehicles = async (req, res) => {
  Customer.findById(req.params.id, (err, customer) => {
    if (err || !customer) {
      return response.UnprocessableEntity(res, 'Unable to retrive customer data!', err)
    }
    customer.populate('vehicles._id', (err) => {
      if (err) {
        return response.UnprocessableEntity(res, 'Unable to retrive customer data!', err)
      }
      return response.Ok(res, 'Received vehicles!', customer.vehicles)
    })
  })
}

exports.viewVehicleById = async (req, res) => {
  await Vehicle.findById(req.params.id, function (err, vehicle) {
    if (err || !vehicle) {
      return response.UnprocessableEntity(res, 'Invalid vehicle id!', err)
    }
    return response.Ok(res, 'Received vehicles!', vehicle)
  })
}
