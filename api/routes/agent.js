module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isAgent } = require('../middlewares/agent')

  const AgentController = require('../controllers/agent')

  app.post('/agent/create_service_category', [Auth, isAgent], AgentController.createServiceCategory)
  app.post('/agent/create_service', [Auth, isAgent], AgentController.createService)
  app.post('/agent/change_appoinment_status', [Auth, isAgent], AgentController.changeAppoinmentStatus)

  app.put('/agent/service/:id', [Auth, isAgent], AgentController.updateService)

  app.delete('/agent/service/:id', [Auth, isAgent], AgentController.deleteService)
}
