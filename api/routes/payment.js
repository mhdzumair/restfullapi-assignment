
module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isAdmin } = require('../middlewares/admin')
  const { isCustomer } = require('../middlewares/customer')
  const { isAdminORAgent } = require('../middlewares/common')

  const PaymentController = require('../controllers/payment')

  app.get('/payments', [Auth, isAdmin], PaymentController.getPayments)
  app.get('/payment/:id', Auth, PaymentController.getPaymentById)
  app.get('/customer_payments/:id', [Auth, isAdmin], PaymentController.getPaymentByCustomerId)
  app.get('/customer_payments', [Auth, isCustomer], PaymentController.getCustomerPayments)
  app.get('/service_payment/:id', [Auth, isCustomer], PaymentController.getPaymentByServiceId)

  app.get('/success', PaymentController.acceptPayment)
  app.get('/cancel', PaymentController.cancelPayment)

  app.post('/init_payment', [Auth, isAdminORAgent], PaymentController.initPayment)
}
