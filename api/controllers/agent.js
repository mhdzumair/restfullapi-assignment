const { Service } = require('../models/service')
const { ServiceCategory } = require('../models/serviceCategory')
const { Appoinment } = require('../models/appoinment')
const response = require('../utils/response')

exports.createServiceCategory = async (req, res) => {
  const newServiceCategory = new ServiceCategory(req.body)
  newServiceCategory.agent = req.agent._id

  newServiceCategory.save((err, serviceCategory) => {
    if (err) {
      return response.NotAcceptable(res, 'Unable to create service Category!', err)
    } else {
      return response.Created(res, 'New service tag is created!', serviceCategory)
    }
  })
}

exports.createService = async (req, res) => {
  const newService = new Service(req.body)
  newService.agent = req.agent._id

  newService.save((err, service) => {
    if (err) {
      return response.NotAcceptable(res, 'Unable to create service Category!', err)
    } else {
      return response.Created(res, 'New service is created!', service)
    }
  })
}

exports.updateService = async (req, res) => {
  await Service.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, function (err, service) {
    if (err || !service) {
      return response.NotFound(res, 'Invalid service id!', err)
    }

    return response.Accepted(res, 'Service updated!', service)
  })
}

exports.deleteService = async (req, res) => {
  await Service.remove({ _id: req.params.id }, function (err, service) {
    if (err || service.deletedCount) {
      return response.NotFound(res, 'Invalid service id!', err)
    }

    return response.Accepted(res, 'Service deleted!')
  })
}

exports.changeAppoinmentStatus = (req, res) => {
  if (!req.body.appoinment_id || !req.body.status) {
    return response.PreconditionFailed(res, 'appoinment_id or status field not found!')
  }
  Appoinment.findById(req.body.appoinment_id, function (err, appoinment) {
    if (err || !appoinment) {
      return response.NotFound(res, 'Invalid Appoinment id!', err)
    }

    appoinment.status = req.body.status

    appoinment.save((err) => {
      if (err) {
        return response.NotAcceptable(res, 'could not change appoinment status', err)
      }
      return response.Accepted(res, 'Appoinment status changed to ' + req.body.status, appoinment)
    })
  })
}
