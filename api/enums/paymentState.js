const PaymentState = {
  created: 'created',
  approved: 'approved',
  failed: 'failed'
}

module.exports = Object.freeze(PaymentState)
