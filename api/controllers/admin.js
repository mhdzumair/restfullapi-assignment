const { Agent } = require('../models/agent')
const { Admin } = require('../models/admin')
const { Customer } = require('../models/customer')
const UserManage = require('../utils/userManage')

exports.getAdmins = (req, res) => {
  UserManage.getUsers(res, Admin)
}

exports.getAdminById = async (req, res) => {
  UserManage.getUser(req, res, Admin)
}

exports.deleteCustomer = async (req, res) => {
  UserManage.deleteUser(req, res, Customer)
}

exports.updateCustomer = async (req, res) => {
  UserManage.updateUser(req, res, Customer)
}

exports.deleteAgent = async (req, res) => {
  UserManage.deleteUser(req, res, Agent)
}

exports.updateAgent = async (req, res) => {
  UserManage.updateUser(req, res, Agent)
}
