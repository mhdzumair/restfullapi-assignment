const PaymentIntent = {
  sale: 'sale',
  approved: 'approved',
  failed: 'failed'
}

module.exports = Object.freeze(PaymentIntent)
