const response = require('./response')

async function searchModelData (res, model, filter) {
  model.find(filter, function (err, doc) {
    if (err) {
      return response.NotFound(res, `Error filteting ${model.modelName}!`, err)
    }
    return response.Ok(res, `Filtered ${model.modelName}!`, doc)
  })
}

module.exports = { searchModelData }
