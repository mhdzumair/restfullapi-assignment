const UserRole = require('../enums/userRoles')
const { Customer } = require('../models/customer')
const response = require('../utils/response')

const isCustomerReg = (req, res, next) => {
  if (req.body.role == null || req.body.role === UserRole.CUSTOMER) {
    next()
  } else {
    response.Forbidden(res, 'No authorization to access this route!')
  }
}

const isCustomer = (req, res, next) => {
  Customer.findOne({ user: req.user._id }, (err, doc) => {
    if (err || !doc) {
      response.Forbidden(res, 'No authorization to access this route!')
    }

    req.customer = doc
    next()
  })
}

module.exports = { isCustomerReg, isCustomer }
