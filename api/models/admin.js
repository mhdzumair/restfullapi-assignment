const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AdminModelSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User field is required!']
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Admin = mongoose.model('Admin', AdminModelSchema)
module.exports = { Admin }
