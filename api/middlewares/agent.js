const { Agent } = require('../models/agent')
const response = require('../utils/response')

const isAgent = (req, res, next) => {
  Agent.findOne({ user: req.user._id }, (err, doc) => {
    if (err || !doc) {
      response.Forbidden(res, 'No authorization to access this route!')
    }

    req.agent = doc
    next()
  })
}

module.exports = { isAgent }
