const UserRole = require('../enums/userRoles')
const response = require('../utils/response')

const isAdminORCustomer = (req, res, next) => {
  if (req.body.role == null || req.body.role === (UserRole.CUSTOMER || UserRole.ADMIN)) {
    next()
  } else {
    response.Forbidden(res, 'No authorization to access this route!')
  }
}

const isAdminORAgent = (req, res, next) => {
  if (req.body.role == null || req.body.role === (UserRole.ADMIN || UserRole.AGENT)) {
    next()
  } else {
    response.Forbidden(res, 'No authorization to access this route!')
  }
}

module.exports = { isAdminORCustomer, isAdminORAgent }
