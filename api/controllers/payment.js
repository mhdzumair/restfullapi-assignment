const paypal = require('paypal-rest-sdk')
const { Payment } = require('../models/payment')
const response = require('../utils/response')
const PaymentState = require('../enums/paymentState')

paypal.configure({
  mode: 'sandbox',
  client_id: process.env.PAYPAL_CLIENT_ID,
  client_secret: process.env.PAYPAL_SECRETE
})

exports.initPayment = (req, res) => {
  const newPayment = Payment(req.body)

  newPayment.validate((err) => {
    if (err) return response.BadRequest(res, err.message)

    const payment = {
      intent: newPayment.intent,
      payer: {
        payment_method: 'paypal'
      },
      redirect_urls: {
        return_url: `http://${process.env.HOST}/api/v1/success`,
        cancel_url: `http://${process.env.HOST}/api/v1/cancel`
      },
      transactions: newPayment.transactions
    }

    paypal.payment.create(payment, async function (error, transaction) {
      if (error) {
        return response.NotAcceptable(res, 'error during payment init', error)
      }
      newPayment._id = transaction.id
      const links = transaction.links

      for (const count in links) {
        if (links[count].method === 'REDIRECT') {
          const paymentLink = links[count].href
          const token = paymentLink.split('token=')[1]
          newPayment.token = token
          await newPayment.save()
          return response.Created(res, `Payment created. send this link to customer ${paymentLink}`, transaction)
        }
      }
    })
  })
}

exports.cancelPayment = (req, res) => {
  Payment.findOneAndUpdate({ token: req.query.token }, { state: PaymentState.failed }, (err, transaction) => {
    if (err || !transaction) return response.NotFound(res, 'token not found', err)

    return response.Ok(res, 'payment canceled')
  })
}

exports.acceptPayment = (req, res) => {
  const paymentId = req.query.paymentId
  const payerId = { payer_id: req.query.PayerID }

  paypal.payment.execute(paymentId, payerId, function (error, payment) {
    if (error || payment.state !== PaymentState.approved) {
      response.NotAcceptable(res, 'Payment Failed', error)
    } else {
      Payment.updateOne({ _id: paymentId }, { state: PaymentState.approved }, (err, transaction) => {
        if (err) return response.NotFound(res, 'cannot update the state of payment', err)
        response.Ok(res, 'Payment Successfull', transaction)
      })
    }
  })
}

exports.getPayments = (req, res) => {
  Payment.find((err, transactions) => {
    if (err || !transactions) {
      response.NotFound(res, 'payments or customer id not found', err)
    }

    return response.Ok(res, 'payments found', transactions)
  })
}

exports.getPaymentById = (req, res) => {
  Payment.findById(req.params.id, (err, transaction) => {
    if (err || !transaction) {
      response.NotFound(res, 'payment not found', err)
    }

    return response.Ok(res, 'payment found', transaction)
  })
}

exports.getPaymentByCustomerId = (req, res) => {
  Payment.find({ customer: req.params.id }, (err, transaction) => {
    if (err || !transaction) {
      response.NotFound(res, 'payments or customer id not found', err)
    }

    return response.Ok(res, 'payment found', transaction)
  })
}

exports.getCustomerPayments = (req, res) => {
  Payment.find({ customer: req.customer._id }, (err, transactions) => {
    if (err || !transactions) {
      response.NotFound(res, 'payments or customer id not found', err)
    }

    return response.Ok(res, 'payments found', transactions)
  })
}

exports.getPaymentByServiceId = (req, res) => {
  Payment.findOne({ service: req.params.id }, (err, transaction) => {
    if (err || !transaction) {
      response.NotFound(res, 'payments or service id not found', err)
    }

    return response.Ok(res, 'payment found', transaction)
  })
}
