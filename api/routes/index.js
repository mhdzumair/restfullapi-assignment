const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  res.send('Welcome to API!')
})

require('./authentication')(router)
require('./admin')(router)
require('./customer')(router)
require('./agent')(router)
require('./common')(router)
require('./payment')(router)

module.exports.router = router
