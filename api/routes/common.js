module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isAdminORAgent, isAdminORCustomer } = require('../middlewares/common')

  const CommonController = require('../controllers/common')

  app.get('/customers', [Auth, isAdminORAgent], CommonController.getCustomers)
  app.get('/agents', [Auth, isAdminORCustomer], CommonController.getAgents)
  app.get('/service_categories', [Auth, isAdminORAgent], CommonController.getAllServiceCategory)
  app.get('/services', [Auth, isAdminORAgent], CommonController.getAllServices)
  app.get('/appoinments', [Auth, isAdminORAgent], CommonController.viewAppoinments)
  app.get('/user', Auth, CommonController.getUserDetails)

  app.get('/appoinment/:id', Auth, CommonController.viewAppoinmentById)
  app.get('/service/:id', [Auth, isAdminORAgent], CommonController.getServiceById)
  app.get('/vehicle/:id', Auth, CommonController.viewVehicleById)
  app.get('/customer/:id', [Auth, isAdminORAgent], CommonController.getCustomerById)
  app.get('/agent/:id', [Auth, isAdminORCustomer], CommonController.getAgentById)

  app.post('/search_customer', [Auth, isAdminORAgent], CommonController.searchCustomer)
  app.post('/search_vehicle', [Auth, isAdminORAgent], CommonController.searchVehicle)

  app.put('/user', Auth, CommonController.updateUserDetails)
}
