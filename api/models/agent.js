const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AgentModelSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User field is required!']
  },
  service_center_address: {
    type: String,
    unique: true,
    required: [true, 'service_center_address field is required!']
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Agent = mongoose.model('Agent', AgentModelSchema)
module.exports = { Agent }
