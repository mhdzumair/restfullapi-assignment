# ABC Service Manger
## Group Name: Dedsec
### Group Members
>> * 18/IT/IT/549    MSM Zumair
>> * 18/IT/IT/474    SH Gallage
>> * 18/IT/IT/477    BAPH Gunasena
>> * 18/IT/IT/504    MNM Nuha
>> * 18/IT/IT/533    SM Sinan


## Requirment Analysis
> ### Purpose
>>This document lists the requirements for the ABC Service Manager project. The purpose of this document is to identify the system requirements and obtain sign-off on all requirements before moving to the design phase.
>
> ### Summary
>
>> **The Problem**
>>> ABC Service Manager needs a system that should be able to
>
>>>> * Store all customers vehicle information and service records.
>
>>>> * Schedule service appointments.
>
>>>> * Search past service records.
>
>>>> * System should be accessed through mobile and web.
>
>> **Key Benefits**
>>> Scalable solution for ABC Service Manager process with centralized data storage, user control, improved UI, and user experience.
>
> ### Project Overview
>>ABC Service Manager is currently using manual system to record all its service center activities. The challenges with the current process are the limited access to the saved data, lack of reporting ability, no access control, and poor user experience due to multiple documents handling.

>>This effort will be to develop new requirements to provide a Web and mobile based application that is to be used by both internal and external users across the ABC Ltd.

> ### Business Requirements

>> * Super Admin:
>>> -This user will have all privileges to administrate the entire vehicle car service center system including access to manage customers and manage service agents.
>> * Service Agents:
>>> -These users will have permission to manage customers service records including approving or rejecting service appointments. Service agents should also be able to view history of the service records.
>> * Customers:
>>> -Customers can register to the system and add their vehicle(s) information to the system. They can also login to the system and create new service appointments and manage vehicle information and service appointments.

>> **Technology Requirements**
>>> * Server/Client programming technology is used to develop this system.
>>> * RESTful APIs are developed using NODEJS.
>>> * MongoDB database is used for database connections.
>>> * Cloud servers are used for deployments.

>> **Hardware Requirements**
>>> * Client side:
>>>> -Browser compatible web based / mobile device
>>> * Server side:
>>>> -Cloud servers. Local environment for development and testing (8GB RAM machine with all necessary tools specified in Front end/back end technology requirement)

>> **Software Requirements**
>>> * Client side:
>>>> -Web Browsers (Chrome, Firefox, Edge), Mobile OS (Android, IOS)
>>> * Server side:
>>>> -All necessary tools needed to develop and test Front end/back end technology requirement

 


## Functionality Diagrams
>User Registration
>
>>	-Customer
>
>>>	*Username
>
>>>	*Email
>
>>>	*Customer ID
>
>>>	*Address
>
>>>	*Password
>

>Employee Login
>> -Super Admin
>
>>> *Admin ID
>
>>> *Entry time
>
>>> *Password
>
>>	-Service Agent
>
>>> *Agent ID
>
>>> *Name
>
>>> *Contact
>
>>>	*Password
>
>Customer Dashboard
>
>> -Edit profile
>
>> -Mention vehicle
>
>>>*Add the vehicle
>
>>>*Edit the vehicle
>
>>>*Delete the vehicle
>
>>-Create new appointment
>
>>>*Appointment ID
>
>>>*Vehicle ID
>
>>>*Service category
>
>>>*Schedule date and time
>
>>-View Service record
>
>>>*Service ID
>
>>>*Service category
>
>>>*Customer ID
>
>>>*Vehicle ID
?
>>>*Date and time
>
>>>*Description
>
>>-Online payment
>
>>>*Bank card number
>
>>>*Receiver card number
>
>>>*Bank name
>
>>>*Branch name
>
>>>*Amount
>
>>-Logout
>
>Super-agent Dashboard
>
>>-Edit profile
>
>>-Service records
>
>>>*Create Service record
>
>>>>`>`Service ID
>
>>>>`>`Service category
>
>>>>`>`Customer ID
>
>>>>`>`Vehicle ID
>
>>>>`>`Date and time
>
>>>>`>`Description
>
>>>*Edit Service record
>
>>>>`>`Service ID
>
>>>>`>`Service category
>
>>>>`>`Customer ID
>
>>>>`>`Vehicle ID
>
>>>>`>`Date and time
>
>>>>`>`Description
>
>>>*Delete Service record
>
>>-Daily appointment
>
>>>*See daily appointment
>
>>>>`>`Approve
>
>>>>`>`Reject
>
>>-Search
>
>>>*Past appointment
>
>>>*Customer details
>
>>>*Customers vehicle details
>
>>>*Service records of vehicles
>
>>-Logout
>
>Admin Dashboard
>
>>-Manage
>
>>>*Customer
>
>>>*Vehicle
>
>>>*Service records
>
>>-Service records
>
>>>*Create Service record
>
>>>>`>`Service ID
>
>>>>`>`Service category
>
>>>>`>`Customer ID
>
>>>>`>`Vehicle ID
>
>>>>`>`Date and time
>
>>>>`>`Description
>
>>>*Edit Service record
>
>>>>`>`Service ID
>
>>>>`>`Service category
>
>>>>`>`Customer ID
>
>>>>`>`Vehicle ID
>
>>>>`>`Date and time
>
>>>>`>`Description
>
>>>*Delete Service record
>
>>-Create service-agent
>
>>>*Name
>
>>>*Contact
>
>>>*Email
>
>>>*NIC
>
>>-Search
>
>>>*Past appointment
>
>>>*Customer details
>
>>>*Customers vehicle details
>
>>>*Service records of vehicles
>
>>-Logout


## ER Diagram

![ER Diagram](https://bitbucket.org/mhdzumair/restfullapi-assignment/raw/96d6c4dc3a16671a0aa7551e116d655588b359bf/images/VehicleAppERdiagram.jpg)

## Models
> UserModel
>> * id
>> * UserName
>> * Name
>> * Email
>> * Password
>> * role
>> * phone_number
>> * created_date

> UserRoles(enum)
>> * CUSTOMER
>> * ADMIN
>> * AGENT

> AppoinmentStatus(enum)
>> * ACCEPTED
>> * DECLINED
>> * PENDING

> CaeColours(enum)
>> * SILVER
>> * WHITE
>> * BLACK
>> * RED
>> * BLUE
>> * YELLOW
>> * GREEN
>> * GRAY
>> * ORANGE

> PaymentIntent(enum)
>> * sale
>> * approved
>> * failed

> paymentStatus(enum)
>> * created
>> * approved
>> * failed

> VehicleTypes(enum)
>> * CAR
>> * VAN
>> * LORRY
>> * BIKE
>> * BUS
>> * THREWEEL

> AdminModel
>> * id
>> * user
>> * created_date

> AgentModel
>> * id
>> * user
>> * service_center_address
>> * created_date

> AppoinmentModel
>> * id
>> * customer
>> * vehicle
>> * service_category
>> * status
>> * schedule_date

> CustomerModels
>> * id
>> * user
>> * vehicles

> PaymentModel
>> * id
>> * intent
>> * transaction
>> * state
>> * customer
>> * service
>> * payment_created_time

> ServiceModel
>> * id
>> * title
>> * description
>> * service_category
>> * customer
>> * vehicle
>> * agent
>> * created_date

> ServiceCategoryModel
>> * id
>> * category
>> * agent
>> * created_date

> VehicleModel
>> * id
>> * vehicle_type
>> * vehicle_model
>> * vehicle_number
>> * color
>> * customer
>> * created_date


## Routes

> AdminRoute
>> * View admins (GET)
>> * view AdiminById (GET)
>> * Update Customer Details (PUT)
>> * Update Agent Details (PUT) 
>> * Delete Customer Details (DELETE)
>> * Delete Agent Details (DELETE)

> AgentRoute
>> * Create ServiceCategory (POST)
>> * Create Service (POST)
>> * Change Appoinment status (POST)
>> * Update Service Details (PUT)
>> * Delete Service Details (DELETE)

> AuthenticationRoute
>> * Register Customer (POST)
>> * Register Agent (POST)
>> * Register Admin (POST)
>> * Login User (POST)
>> * View User Details (GET)

> CommonRoute
>> * View Customers (GET)
>> * View Agent (GET)
>> * View All service categories (GET)
>> * View All services (GET)
>> * View Appoinments (GET)
>> * View AppoinmentById (GET)
>> * View SreviceById (GET)
>> * View VehiclById (GET)
>> * View CustomerById (GET)
>> * View AgentByID (GET)
>> * Search Customer (POST)
>> * Search Vehicle (POST)

> CustomerRoute
>> * View Vehicles (GET)
>> * View Appoinments (GET)
>> * View Customer Services (GET)
>> * View AppoinmentById (GET)
>> * Add Vehicle (POST)
>> * Search Service Categories (POST)
>> * Add Appoinments (POST)
>> * Update Vehicle (PUT)
>> * Delete Vehicle Details (DELETE)
>> * Cancel Appoinments (DELETE)

> PaymentRoute
>> * View Payments (GET)
>> * View PaymentById (GET)
>> * View PaymentByCustomerId (GET)
>> * View Customer payments (GET)
>> * View PaymentByServiceId (GET)
>> * View Accept Payment (GET)
>> * View Cancel Payment (GET)
>> * Init Payment (POST)





