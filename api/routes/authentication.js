module.exports = function (app) {
  const { Auth } = require('../middlewares/authentication')
  const { isCustomerReg } = require('../middlewares/customer')
  const { isAdmin } = require('../middlewares/admin')

  const AuthController = require('../controllers/authentication')

  app.post('/register', isCustomerReg, AuthController.registerCustomer)
  app.post('/register_agent', [Auth, isAdmin], AuthController.registerAgent)
  app.post('/register_admin', [Auth, isAdmin], AuthController.registerAdmin)
  app.post('/login', AuthController.loginUser)
}
