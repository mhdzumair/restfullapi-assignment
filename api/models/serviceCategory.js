const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ServiceCategoryModelSchema = new Schema({
  category: {
    type: String,
    required: [true, 'category field is required!'],
    unique: true
  },
  agent: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Agent',
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const ServiceCategory = mongoose.model('ServiceCategory', ServiceCategoryModelSchema)
module.exports = { ServiceCategory }
