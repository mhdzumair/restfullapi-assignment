const mongoose = require('mongoose')
const PaymentIntent = require('../enums/paymentIntent')
const PaymentState = require('../enums/paymentState')

const Schema = mongoose.Schema

const PaymentModelSchema = new Schema({
  _id: String,
  intent: {
    type: String,
    enum: PaymentIntent,
    required: [true, 'intent field is required!']
  },
  transactions: {
    type: [Map],
    required: [true, 'transaction field is required']
  },
  state: {
    type: String,
    enum: PaymentState,
    default: PaymentState.created
  },
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
    required: [true, 'customer field is required!']
  },
  service: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Service',
    required: [true, 'service field is required!']
  },
  payment_created_time: {
    type: Date,
    default: Date.now
  },
  token: String
})

PaymentModelSchema.plugin(require('mongoose-id-validator'))
const Payment = mongoose.model('Payment', PaymentModelSchema)
module.exports = { Payment }
