const mongoose = require('mongoose')

const Schema = mongoose.Schema

const ServiceModelSchema = new Schema({
  title: {
    type: String,
    required: [true, 'Title field is required!']
  },
  description: {
    type: String,
    required: [true, 'Description field is required!']
  },
  service_category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ServiceCategory',
    required: [true, 'service_category field is required!']
  },
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
    required: true
  },
  vehicle: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vehicle',
    required: true
  },
  agent: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Agent',
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

ServiceModelSchema.index({
  title: 'text',
  description: 'text'
}, {
  weights: {
    title: 3,
    description: 5
  }
})

ServiceModelSchema.plugin(require('mongoose-id-validator'))
const Service = mongoose.model('Service', ServiceModelSchema)
module.exports = { Service }
